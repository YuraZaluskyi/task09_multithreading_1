package com.zaluskyi.fibonacciNumbers.controller;

import com.zaluskyi.fibonacciNumbers.model.Fibonacci;

import java.util.Scanner;

public class Controller {
    private Fibonacci fibonacci = new Fibonacci();
    private Scanner input = new Scanner(System.in);


    public void enterNumber() {
        int number = 1;
        System.out.println("Enter number");
        number = input.nextInt();
        fibonacci = new Fibonacci(number);
        fibonacci.setCount(number);
    }

    public void startFibonacciOneThread() {
        fibonacci.run();
    }

    public void startFibonacciMultiThread() {
        for (int i = 0; i < fibonacci.getCount(); i++) {
            new Thread(new Fibonacci(i + 1)).start();
        }
    }
}
