package com.zaluskyi.fibonacciNumbers;

import com.zaluskyi.fibonacciNumbers.view.View;

public class Main {
    public static void main(String[] args) {
        new View().start();
    }
}
