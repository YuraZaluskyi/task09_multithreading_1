package com.zaluskyi.fibonacciNumbers.model;

public interface Generator<T> {
    public T next();
}
