package com.zaluskyi.fibonacciNumbers.view;

import com.zaluskyi.fibonacciNumbers.controller.Controller;

import java.util.Scanner;

public class View {
    private Controller controller = new Controller();
    Scanner input = new Scanner(System.in);

    public void start() {
        String choice;
        while (true) {
            menu();
            choice = input.next();
            switch (choice) {
                case "1":
                    break;
                case "2":
                    break;
                default:
                    return;
            }
        }
    }

    private void menu() {
        System.out.println("    M E N U");
        System.out.println("1 - enter count number\n2 - print fibonacci number in one thread\n" +
                "3 - print multiThread number fibonacci\nEXIT - press any number");
    }
}
