package com.zaluskyi.pingpongproject;

import com.zaluskyi.pingpongproject.view.View;

public class Main {
    public static void main(String[] args) {
        new View().myView();
    }
}
