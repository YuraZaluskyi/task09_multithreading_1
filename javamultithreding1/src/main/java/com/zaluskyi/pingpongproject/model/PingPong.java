package com.zaluskyi.pingpongproject.model;

public class PingPong {
    private final static Object obj = new Object();
    private String firstWord = "PING";
    private String secondWord = "PONG";

    public void play() {
        Thread firstThread = new Thread(
                () -> {
                    synchronized (obj) {
                        while (true) {
                            try {
                                Thread.sleep(1000);
                                obj.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            System.out.println(firstWord);
                            obj.notify();
                        }
                    }
                });

        Thread secondThread = new Thread(
                () -> {
                    synchronized (obj) {
                        while (true) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            obj.notify();
                            try {
                                obj.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            System.out.println(secondWord);
                        }
                    }
                });
        firstThread.start();
        secondThread.start();
    }
}
