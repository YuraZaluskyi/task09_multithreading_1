package com.zaluskyi.pingpongproject.view;

import com.zaluskyi.pingpongproject.controller.Controller;

import java.util.Scanner;

public class View {
    private Controller controller = new Controller();
    private Scanner input = new Scanner(System.in);

    public void myView() {
        String choice;
        while (true) {
            menu();
            choice = input.next();
            switch (choice) {
                case "1":
                    controller.startPlay();
                    break;
                default:
                    controller.gameOver();
                    return;
            }
        }
    }

    private void menu() {
        System.out.println("    M E N U");
        System.out.println("1 - start ping-pong\nEXIT - press any key");
    }
}
