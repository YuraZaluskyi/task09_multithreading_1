package com.zaluskyi.pingpongproject.controller;

import com.zaluskyi.pingpongproject.model.PingPong;

import java.util.Scanner;

public class Controller {
    private PingPong pingPong = new PingPong();
    private Scanner input = new Scanner(System.in);

    public void startPlay() {
        pingPong.play();
    }

    public void gameOver() {
        System.out.println("goodbye");
    }
}
